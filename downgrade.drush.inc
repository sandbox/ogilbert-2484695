<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Blobsmith
 * Date: 25/04/15
 * Time: 19:58
 * To change this template use File | Settings | File Templates.
 */

/**
 * Implements hook_drush_command().
 *
 * @return array
 */
function downgrade_drush_command(){
    $items = array();

    $items['downgrade-down'] = array(
        'command hook' => 'downgrade_down',
        'description' => 'Downgrade a module to the previous version',
        'arguments' => array(
            'module_machine_name' => 'The machine name of the module.',
        ),
        'options' => array(
            'number' => 'The number of module version to downgrade (ex: 7005 to 7003 if you specify 2)',
            'force' => 'Force the downgrade if the module have not hook_downgrade defined for the version checked '
        ),
        'examples' => array(
            'drush downgrade-down migrate --number=2 --force' => 'Downgrade the module migrate of 2 versions and force the downgrade.',
            'drush ddown migrate,block' => 'Downgrade a list of modules',
        ),
        'aliases' => array('ddown'),
        'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL
    );

    return $items;
}

/**
 * Downgrade all modules set to $module_name parameter
 *
 * @param null $module_name
 */
function drush_downgrade_down($module_name = null){
    // Check parameters
    if(!$module_name){
        drush_set_error('DRUSH_FRAMEWORK_ERROR', dt('You have to specified a module name.'));
    }

    $force = drush_get_option('force', 0);
    $number = drush_get_option('number', 1);

    if($force){
        if(!drush_confirm(dt('Are you sure you want to force the downgrade ?'), 0)){
            drush_set_error('DRUSH_FRAMEWORK_ERROR', dt('Downgrade aborted.'));
        }
    }

    // Check errors
    $errors = drush_get_error_log();
    if(empty($errors)){
        if(strpos($module_name, ',')){
            $module_name = explode(',', $module_name);
        }
        downgrade_downgrade_modules($module_name, $number, $force);
    }
    else{
        drush_user_abort();
    }
}